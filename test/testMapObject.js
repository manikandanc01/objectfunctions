const mapObjectFunction = require("../mapObject");
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

//Callback function add 5 to any value given by the object
const callback = function (value) {
  return value + 5;
};

const result = mapObjectFunction(testObject, callback);
console.log(result);

function values(obj) {
  let result = [];

  for (const key in obj) {
    if (typeof obj[key] != "function") {
      result.push(obj[key]);
    }
  }

  return result;
}
module.exports = values;

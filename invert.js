function invert(obj) {
  let copyObject = {};
  for (const key in obj) {
    copyObject[obj[key]] = key;
  }
  return copyObject;
}
module.exports = invert;

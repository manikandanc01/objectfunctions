function mapObject(obj, cb) {
  let result = [];
  for (const key in obj) {
    result.push(cb(obj[key]));
  }
  return result;
}
module.exports = mapObject;

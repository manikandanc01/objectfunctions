function pairs(obj) {
    let result=[];
    for(const key in obj){
        result.push([key,obj[key]]);
    }
    return result;
}
module.exports=pairs;
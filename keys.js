function keys(obj) {
  let keysArray = [];

  for (const objKey in obj) {
    keysArray.push(objKey.toString());
  }

  return keysArray;
}
module.exports = keys;

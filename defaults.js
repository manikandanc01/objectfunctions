function defaults(obj, defaultProps) {
  for (const props in defaultProps) {
    if (obj[props] === undefined) {
      obj[props] = defaultProps[props];
    }
  }

  return obj;
}
module.exports=defaults;
